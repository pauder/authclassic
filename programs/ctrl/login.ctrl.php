<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once 'base.php';

/**
 *
 */
class AuthClassic_CtrlLogin extends AuthClassic_Controller
{
    public function displayForm($title = null, $errorMessages = null, $referer = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        if ($title) {
            $page->addItem($W->Title($title, 2)->setId('authclassic-title'));
        }
        if ($errorMessages) {
            $errors = explode("\n", $errorMessages);
            foreach ($errors as $errorMessage) {
                $page->addItem($W->Label($errorMessage)->addClass('authclassic-error-label'));
            }
        }
        $page->addItemMenu('signon', bab_translate("Login"), '');
        $page->setCurrentItemMenu('signon');

        $settings = bab_getInstance('bab_Settings');
        /*@var $settings bab_Settings */
        $site = $settings->getSiteSettings();

        if($site['registration'] == 'Y') {
            $page->addItemMenu('register', bab_translate("Register"), $GLOBALS['babUrlScript'].'?tg=login&cmd=register');
        }


        require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';
        if(bab_isEmailPassword())
        {
            $page->addItemMenu('emailpwd', bab_translate("Lost Password"), $GLOBALS['babUrlScript'].'?tg=login&cmd=emailpwd');
        }


        /*if (BAB_AUTHENTIFICATION_OVIDENTIA !== (int) $babBody->babsite['authentification'])
        {
            AuthClassic_Error(AuthClassic_translate('The authentication must be configured to use the ovidentia database while using the AuthClassic addon'));
            return false;
        }*/

        $func = bab_functionality::get('PortalAuthentication/AuthClassic');
        /* @var $func Func_PortalAuthentication_AuthClassic */
        $page->addItem($func->getAuthForm());

        return $page;
    }
    

    public function login()
    {
        $func = bab_functionality::get('PortalAuthentication/AuthClassic');
        /*@var $func Func_PortalAuthentication_AuthClassic */
        if (true === $func->login()) {
            // force page redirect because dialog close and reload will not refresh the connexion status on page
            die('<p>'.AuthClassic_translate('You are now logged in...').'</p><script type="text/javascript"> document.location.reload(); </script>');
        }
        
        // this will close dialog
        die();
    }
}